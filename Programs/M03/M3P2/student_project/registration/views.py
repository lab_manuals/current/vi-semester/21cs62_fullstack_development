# registration/views.py

from django.shortcuts import render, redirect
from .forms import StudentForm, ProjectForm
from .models import Student

def register_student(request):
    if request.method == 'POST':
        student_form = StudentForm(request.POST)
        project_form = ProjectForm(request.POST)
        if student_form.is_valid() and project_form.is_valid():
            project = project_form.save()
            student = student_form.save(commit=False)
            student.project = project
            student.save()
            return redirect('student_list')
    else:
        student_form = StudentForm()
        project_form = ProjectForm()
    return render(request, 'registration/register_student.html', {'student_form': student_form, 'project_form': project_form})

def student_list(request):
    students = Student.objects.all()
    return render(request, 'registration/student_list.html', {'students': students})

