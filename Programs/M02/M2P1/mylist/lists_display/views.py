# lists_display/views.py

from django.shortcuts import render

def display_lists(request):
    fruits = ['Apple', 'Banana', 'Mango', 'Jackfruit', 'Grapes']
    students = ['Jamal', 'Suresh', 'Charlie', 'Divya', 'Raja']
    context = {
        'fruits': fruits,
        'students': students,
    }
    return render(request, 'lists_display/display_lists.html', context)

