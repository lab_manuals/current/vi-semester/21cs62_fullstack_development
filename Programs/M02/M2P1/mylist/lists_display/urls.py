# lists_display/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('display_lists/', views.display_lists, name='display_lists'),
]
