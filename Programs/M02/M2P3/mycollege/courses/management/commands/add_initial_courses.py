# courses/management/commands/add_initial_courses.py

from django.core.management.base import BaseCommand
from courses.models import Course

class Command(BaseCommand):
    help = 'Add initial courses to the database'

    def handle(self, *args, **kwargs):
        courses = [
            {'name': 'Mathematics', 'description': 'Basic Mathematics'},
            {'name': 'Physics', 'description': 'Fundamentals of Physics'},
            {'name': 'Chemistry', 'description': 'Basics of Chemistry'},
            {'name': 'Biology', 'description': 'Introduction to Biology'},
            {'name': 'Computer Science', 'description': 'Basics of Computer Science'},
        ]

        for course in courses:
            Course.objects.get_or_create(name=course['name'], defaults={'description': course['description']})

        self.stdout.write(self.style.SUCCESS('Successfully added initial courses'))

