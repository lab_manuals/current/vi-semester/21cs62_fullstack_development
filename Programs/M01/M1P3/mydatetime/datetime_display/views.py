# datetime_display/views.py

from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

def current_datetime(request):
    now = datetime.now()
    context = {
        'current_date': now,
    }
    return render(request, 'datetime_display/current_datetime.html', context)
