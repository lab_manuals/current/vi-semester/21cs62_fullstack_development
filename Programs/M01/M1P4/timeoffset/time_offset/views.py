# time_offset/views.py

from django.shortcuts import render
from django.utils import timezone
from datetime import timedelta

def current_datetime(request):
    now = timezone.now()
    four_hours_ahead = now + timedelta(hours=4)
    four_hours_before = now - timedelta(hours=4)
    context = {
        'current_date': now,
        'four_hours_ahead': four_hours_ahead,
        'four_hours_before': four_hours_before,
    }
    return render(request, 'time_offset/current_datetime.html', context)
