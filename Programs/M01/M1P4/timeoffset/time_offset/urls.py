# time_offset/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('time_offsets/', views.current_datetime, name='current_datetime'),
]
