from django.apps import AppConfig


class TimeOffsetConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "time_offset"
