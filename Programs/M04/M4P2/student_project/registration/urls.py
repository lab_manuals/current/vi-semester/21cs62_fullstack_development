# registration/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register_student, name='register_student'),
    path('students/', views.StudentListView.as_view(), name='student_list'),
    path('students/<int:pk>/', views.StudentDetailView.as_view(), name='student_detail'),
    path('students/csv/', views.generate_csv, name='generate_csv'),
    path('students/pdf/', views.generate_pdf, name='generate_pdf'),
]

