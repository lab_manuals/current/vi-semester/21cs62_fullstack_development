# registration/views.py

import csv
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Student, Project
from .forms import StudentForm, ProjectForm
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

def register_student(request):
    if request.method == 'POST':
        student_form = StudentForm(request.POST)
        project_form = ProjectForm(request.POST)
        if student_form.is_valid() and project_form.is_valid():
            project = project_form.save()
            student = student_form.save(commit=False)
            student.project = project
            student.save()
            return redirect('student_list')
    else:
        student_form = StudentForm()
        project_form = ProjectForm()
    return render(request, 'registration/register_student.html', {'student_form': student_form, 'project_form': project_form})

class StudentListView(ListView):
    model = Student
    template_name = 'registration/student_list.html'
    context_object_name = 'students'

class StudentDetailView(DetailView):
    model = Student
    template_name = 'registration/student_detail.html'
    context_object_name = 'student'

def generate_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="students.csv"'

    writer = csv.writer(response)
    writer.writerow(['First Name', 'Last Name', 'Email', 'Project Topic', 'Languages Used', 'Duration'])

    students = Student.objects.all().select_related('project')
    for student in students:
        writer.writerow([student.first_name, student.last_name, student.email, student.project.topic, student.project.languages_used, student.project.duration])

    return response

def generate_pdf(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="students.pdf"'

    buffer = BytesIO()
    p = canvas.Canvas(buffer, pagesize=letter)
    p.setFont("Helvetica", 12)

    students = Student.objects.all().select_related('project')
    y = 750
    p.drawString(30, y, "First Name")
    p.drawString(130, y, "Last Name")
    p.drawString(230, y, "Email")
    p.drawString(330, y, "Project Topic")
    p.drawString(430, y, "Languages Used")
    p.drawString(530, y, "Duration")

    for student in students:
        y -= 30
        p.drawString(30, y, student.first_name)
        p.drawString(130, y, student.last_name)
        p.drawString(230, y, student.email)
        p.drawString(330, y, student.project.topic)
        p.drawString(430, y, student.project.languages_used)
        p.drawString(530, y, student.project.duration)

    p.showPage()
    p.save()

    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='students.pdf')

