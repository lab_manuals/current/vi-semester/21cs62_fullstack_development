# registration/views.py

from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from .models import Student
from .forms import StudentForm, ProjectForm

def register_student(request):
    if request.method == 'POST':
        student_form = StudentForm(request.POST)
        project_form = ProjectForm(request.POST)
        if student_form.is_valid() and project_form.is_valid():
            project = project_form.save()
            student = student_form.save(commit=False)
            student.project = project
            student.save()
            return redirect('student_list')
    else:
        student_form = StudentForm()
        project_form = ProjectForm()
    return render(request, 'registration/register_student.html', {'student_form': student_form, 'project_form': project_form})

class StudentListView(ListView):
    model = Student
    template_name = 'registration/student_list.html'
    context_object_name = 'students'

class StudentDetailView(DetailView):
    model = Student
    template_name = 'registration/student_detail.html'
    context_object_name = 'student'

